*Project connect 4*

![](pictures/puissance4.png)


This project has been created a long time ago. It's a connect 4 application
in java who use local network to operate a party.
We have used [Scene builder](https://gluonhq.com/products/scene-builder/) to create the different views.

############### Installation ##############

```
git clone git@gitlab.com:arabakevin/puissance_4_java.git
```
############### to play ##############

go to the folder named `dist` and click on the Puissance4.jar

It's necessary to play with two players ( on two different computers )
to play at this game.

* The first create the party and the second join the first one party already
create with the same coordinates.

* And the first player start and the party is going
